
#title: "05_Comparison_taxonomic_assignation"
#author: "Thomas Finet"
#date: "30/11/2022"

#Load the libraires that we will use 
library(dplyr)
library(stringr)
library(readr)


#Import data 


#Call the ASV table 
ASTAN_ASV_PR2<-read_table(here::here("Data/ASTAN_ASV.tsv.gz"))
#Call the newly assigned table 

##Table where the classifier has only been trained with the choanoflagellates reference sequences 
ASTAN_ASV_EuKribo_1<-read.csv(here::here("Output/Table_ASTAN_new_assign.csv"))
##Table where the classifier has been trained with the Eukaryotes reference sequences
ASTAN_ASV_EuKribo_2<-read.csv(here::here("Output/Table_ASTAN_new_assign_2.csv"))


#Filter the Choanoflagellatea clade 


#filter the Choanozoa clade
ASTAN_ASV_PR2_filtered <-ASTAN_ASV_PR2 %>% 
  filter(Class=="Choanoflagellatea")


#Modify the newly assigned table

#function to process the tables from the 2 Eukribo assignation

Eukribo_genus<-function(X,i){
  #Extract from the taxonomy the identification at the genus level
  Y<-sub("^.+g:([^;]+).+$","\\1",X$taxo)
  #Create a dataframe to store the amplicon, and identification value 
  X<-X %>% 
    select(amplicon) %>% 
    mutate(c=Y) %>%
    #Fill the row with a NA if there is no assignation at the genus level 
    mutate(c=ifelse(str_detect(c,"Root"),NA,c))
  #Rename the column containing the assignation
  colnames(X)[2]<-paste("EuKribo_genus",i, sep="_")
  
  return(X)
 }

#We apply the function for EuKribo1
ASTAN_ASV_EuKribo_genus_ChoanoTrain<-Eukribo_genus(ASTAN_ASV_EuKribo_1,1)
#We apply the function for EuKribo2
ASTAN_ASV_EuKribo_genus_All<-Eukribo_genus(ASTAN_ASV_EuKribo_2,2)
#We merge the tables with the identification EuKribo1 and EuKribo2 
ASTAN_ASV_EuKribo_all<-left_join(ASTAN_ASV_EuKribo_genus_ChoanoTrain,ASTAN_ASV_EuKribo_genus_All)


#Modify the PR2-assigned data 


#In the original table, taxonomically annotated using PR2,we select the column of interest, i.e. Genus and amplicon
ASTAN_ASV_PR2_genus<-ASTAN_ASV_PR2_filtered %>% 
  select(PR2_Genus=Genus,amplicon) 

#Replace the values with"_X"(=unidentified) by NA
ASTAN_ASV_PR2_genus[which(str_detect(ASTAN_ASV_PR2_genus$PR2_Genus,"_X")),1] <- NA


#Merge the 2 tables by amplicon 


#We then merge the table with the PR2 assignation and the table with the EuKribo assignations 
Taxonomy_Choano<-left_join(ASTAN_ASV_EuKribo_all,ASTAN_ASV_PR2_genus)
#For the clarirty of our oral presentation we removed the first identification using only Choanoflagellates 
Taxonomy_Choano<-Taxonomy_Choano %>% 
  select(-EuKribo_genus_1)

#Export the taxonomy data
write.csv(Taxonomy_Choano,here::here("Output/Taxonomy_Choano.csv"))

#Compare the assignations 
table_comparison<-table(Count_EuKribo=!is.na(Taxonomy_Choano$EuKribo_genus_2),
                        Count_PR2=!is.na(Taxonomy_Choano$PR2_Genus))


