#title: Data importation
#author: Thomas Finet
#date: 28/11/2022


#Load tidyverse 
library(tidyverse)

#Create Directories

#Create the directory where we will store the data 
if (!dir.exists(here::here("Data"))){
  dir.create(here::here("Data"))
}
#Create the directory where we will store our output
if (!dir.exists(here::here("Output"))){
  dir.create(here::here("Output"))
}


#download the ASV table 
download.file("https://mycore.core-cloud.net/index.php/s/P78kb7vtsbVd1om/download",destfile =here::here("Data/ASTAN_ASV.tsv.gz")) 
#Download the Eukribo reference database (10.5281/zenodo.6896896)
download.file("https://zenodo.org/record/6896896/files/46346_EukRibo-02_full_seqs_2022-07-22.fas.gz",destfile =here::here("Data/EuKribo.fas.gz"))


