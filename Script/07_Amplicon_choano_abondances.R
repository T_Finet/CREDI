##title: "Abd relative et absolue des Amplicons"
##author: Baptiste OZANAM

library(tidyverse)

# data import ####

#Data frame ASTAN avec ASV de PR2
datatot <- read_table(here::here("Data/ASTAN_ASV.tsv.gz"))


#Mise en forme des données totales
datatot <- datatot %>%
  select(-(identity_Kingdom:identity_Species))%>% #nettoyage des colonnes qui ne sont pas utilisées
  select(-(Kingdom:Division)) %>% #nettoyage des colonnes qui ne sont pas utilisées 2
  select(-(Order:Species)) %>% #nettoyage des colonnes qui ne sont pas utilisées 3
  dplyr:::filter( Class =="Choanoflagellatea") %>% #sélection des lignes de la classe des Choanoflagellées
  select(amplicon,total) %>% #sélection des colonnes d'intérêt
  mutate(Abd_tot = (sum(total))) %>% #calcul de l'abondance totale des amplicons
  mutate(Abd_rel=(total/Abd_tot)*100) #calcul de l'abondance relative de chaque amplicon

names(datatot)[names(datatot) == 'total'] <- 'Abd_0'

#Export du Data frame
write.csv(datatot, file = "Output/amplicon_choano_abondances.csv")





