# Choanoflagellates in Roscoff coastal waters: an Exploratory analysis of their DIversity (CREDI) 

Ilaria Barale (ilariabarale14@gmail.com), Thomas Finet (thomas-finet@orange.fr) & Batipste Ozanam (baptiste.ozanam@gmail.com)

## Résumé

Les Choanoflagellés sont des protistes ubiquistes présent à toutes les latitudes dans les habitats pélagiques et benthiques. Peu étudié, leur rôle écosystémique est anticipé comme étant majeur. En effet, ces derniers seraient capable de filtrer chaque jour jusqu'à 25% des eaux côtières impliquant d'importants transferts de matière et d'énergie vers les niveaux trophiques supérieurs. Ce projet vise donc à étudier ce groupe taxonomique dans les eaux côtières de Roscoff en **(i)** établissant une liste de taxons, **(ii)** décrivant leur phénologie et **(iii)** en comparant la communauté roscovite à celles d'autres sites européens.

## Données

Nous disposons de données de metabarcoding issues du suivi temporel mené sur la période 2009-2016 à la station ['ASTAN'](https://zenodo.org/record/5032451#.Y404g3bMK3A), située au large de Roscoff. Un échantillonnage bi-mensuel y a été effectué. 

Les bases de données de références pour l'assignation taxinomique utilisées dans cette étude sont les suivantes :
  
- [EukRIbo](https://zenodo.org/record/6327891#.Y407F3bMK3A)
- [PR2](https://pr2-database.org/)

Nous avons travaillé à partir [d'une table d'ASV](https://mycore.core-cloud.net/index.php/s/P78kb7vtsbVd1om/download) préalablement assignée taxonomiquement à l'aide de PR2 (18S v4.13). L'ensemble des étapes du traitement des données utilisées pour produire cette table d'ASV sont détaillées [ici](https://github.com/benalric/MetaB_pipeline)


## Outils

- R version 4.1.1 (2021-08-10)
- Platform: x86_64-conda-linux-gnu (64-bit)
- Running under: Ubuntu 18.04.6 LTS
- Mobaxterm (version 22.2)
- package DECIPHER (version 2.22.0)

## Recommandation

Les scripts doivent être exécutés dans cette ordre :

![Organigramme des input et output de chaque script du projet CREDI](Figures/schema_input_output.png)

Afin de faciliter l'analyse, nous avons crée le script [launcher.R](Script/launcher.R) qui permet d'éxecuter l'ensemble des scripts dans le bon ordre.

# Journal de bord

## 29/11/2022

**Objectif** : prise en main des données de metabarcoding, familiarisation avec la méthodologie de récolte et de traitement des données de metabarcoding, début des analyses 

**Réalisations** : 
- Lecture de l'article de  [Caracciolo et al.,2022](https://onlinelibrary.wiley.com/doi/full/10.1111/mec.16539) afin de se familiariser avec le jeu de données
- Mise au point d'un schéma synthétisant la méthodologie d'acquisition et de traitement des données utilisées pour encoder notre jeu de données : ![Schéma](Figures/schema_data_pipeline.png)

- Création du fichier 01_Data_Import.Rmd > script pour importer les données depuis le cloud "https://zenodo.org/record/6896896/files/46346_EukRibo-02_full_seqs_2022-07-22.fas.gz"

- Création du fichier 02_Fasta_file_Choano.R > script pour filtrer les choanoflagellés et exporter les séquences ADN au format FASTA

- Création du fichier 03_Training_Taxonomy_Choanoflagellate.R > script pour entrainer un classifier à partir des données issues de la base de données EuKribo : "https://zenodo.org/record/6896896"

- Création du fichier 04_NewAssignation_Choanoflagellates.R > script pour réassigner la taxinomie des Choanoflagellés à l’aide d’un classifier, entrainé à partir de la base de données EukRibo.

- Création du fichier Script Ila.Rmd > Mise en forme des données (filter sur les Choanoflagellées, pivot du data frame, changement du format de la date, abondances) pour pouvoir tracer les graphiques liés à l'étude phénologique

**Idées** : mettre en forme le document des nouvelles assignations dans un format exploitable ; comparer les nouvelles et les anciennes assignations ; terminer de travailler sur les indices phénologiques des choanoflagellés

## 30/11/2022

**Objectif** : Exporter les nouvelles assignations taxinomiques dans un format pratique ; Comparaison des deux méthodes d’assignation taxinomique ; Étude de l'évolution des Choanoflagellés à l'échelle temporelle

**Réalisations** : 

<span class="bg-info"> Phénologie </span>

- Finalisation du script dédié à la création d'un data frame avec les abondances relatives des Chonoflagellées.

<span class="bg-info"> Liste des taxons  </span>

- Creation du fichier 05_Comparison_assignation.Rmd qui permet de comparer les résultats d'assignation avec PR2 ou EukRibo par amplicon

- Etude des résultats de l'annotation taxinomique à l'aide d'IdTaxa entrainé sur les séquences de Choanoflagellés contenues dans EukRibo : on observe que ce n'est pas la même taxonomie qui est utilisée dans l'annotation avec EukRibo par rapport à l'annotation avec PR2. 

- Duplication des scripts 3 et 4 afin de faire tourner IdTaxa avec l'ensemble des séquences de EukRibo, le calcul a été effectué sur le cluster de grâce au script : 03_Training_Taxonomy_Bigger_launcher.bash

<span class="bg-info"> Autres notes </span>

- Changement de nom du fichier Script Ila.Rmd -> 06_Dataframe_for_figure_PR2.R

**Idées** : Pour expliquer la différence dans les taxonomies utilisées, il serait intéressant d'aller chercher les séquences de reférences selon les versions des bases utilisées. Dans la soirée nous avons réentrainé IdTaxa avec l'ensemble de la base de référence EukRibo, il sera interessant de comparer la différence d'assignation entre les deux versions.

## 01/12/2022

**Objectif** : Terminer les analyses et manipulation de données afin de pouvoir créer et mettre en forme des graphiques synthétiques

**Réalisations** :

<span class="bg-info"> Phénologie </span>

Une première figure représentant le nombre de reads total et ceux appartenant aux Choanoflagellés en fonction du temps a été réalisée, mais cette dernière n'est pas très lisible. Les Choanoflagellés ne représentent effectivement que 4% du pool de reads total : on ne voit donc pas très bien les variations d'abondance de ces reads. 

![Évolution du nombre total de reads en fonction du temps](Figures/reads_tps.PNG)

![Évolution du nombre total de reads affiliés à des choanoflagellés en fonction du temps](Figures/read_choano_tps.PNG)

Une seconde figure a été réalisée. Cette dernière représente la variation d'abondance des genres de Choanoflagellés (nom issus de l'identification de Eukribo) en fonction des mois et des années. Le nombre de stations (n = 168) et de genres (n = 24) rendait cette dernière très peu lisible. Il a donc été décidé d'aggréger les données par genre (majeurs et mineurs) et par mois et annéem en moyennant les abondances relatives

![Somme des abondances relatives des Choanoflagellés sur l’abondance relative totale en fonction du temps](Figures/abd_relative_01.PNG) 

![Abondances relatives des Choanoflagellés en fonction du temps](Figures/abd_relative_02.PNG)

Pour pouvoir comparer nos résultats avec de potentielles autres études sur les choanoflagellés, nous avons ensuite créé le script "09_timeline_main_choano.R" pour tracer l'évolution de l'abondance relatives des 3 genres de choanoflagellés les plus importants (en %) à l'échelle de la totalité de la communauté planctonique échantillonnée. 

![Abondances relatives des 3 principaux genres de Choanoflagellés en fonction du temps](Figures/Abondance_Genus_Top3.png)

<span class="bg-info"> Liste des taxons </span>
- Au cours de la matinée nous avons pu échanger avec Cédric Berney, auteur de la base de donnée EukRibo. 
- Un second assignement de taxons a été réalisé en utilisant,cette fois-ci, toute la base de donnée Eukribo. Les différences sont les suivantes : Le nombre d'assignation n'est pas le même et certaines séquences sont assignées à des champignons lorsque l'on entraine IdTaxa sur l'ensemble des Eucaryotes. Les résultats problématiques ont été vérifiés par des blasts sur NCBI. 

- Nous avons ensuite choisi de nous concentrer sur les assignations taxinomiques obtenues lorsque IdTaxa est entrainé sur l'ensemble de la base de donnée EukRibo. 
- Afin de représenter cela nous avons crée le script 08_Plot_rangs.Rmd

Pour pouvoir représenter graphiquement ces différentes assignations, nous avons décidé de réaliser un diagramme rang-abondances relatives des ASVs de Choanoflagellés identifié à l’échelle du genre par PR2 et Eukribo


![Diagramme rang-abondance relative des ASVs de Choanoflagellés identifié à l’échelle du genre.](Figures/diagramme rang-abondance.PNG)


<span class="bg-info"> Autres notes </span>

- Changement de nom du fichier 07_dataforfigure_Eukrib.R -> 07_amplicon_choano_abondances.R
- 05/12 : Creation d'un fichier launcher pour lancer l'ensemble des scripts à partir d'un seul 





