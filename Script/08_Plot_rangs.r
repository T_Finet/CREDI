
#title: "08_comparison_taxonomic_assignation"
#author: "Thomas Finet"
#date: "01/12/2022"


#Load the libraries 
library(dplyr)
library(ggplot2)

## Import files 

#Import the taxonomic identification of the amplicons
Taxonomy<-read.csv("Output/Taxonomy_Choano.csv")
#Import the abundances of the ASV
Choano_abundance<-read.csv("Output/amplicon_choano_abondances.csv")


#Merge both files and compute cumulative Sum

#Create the table that we will use for our plot 
##Join the taxonomy and the relative abundances tables
Table_plot<-left_join(Choano_abundance,Taxonomy) %>%
  #Arrange the table by decreasing order
  arrange(-Abd_rel) %>%
  #Compute the cumulative Sum
  mutate(CumSumAb=cumsum(Abd_rel),
         #Attribute a rank to the ASV, based on their abundances 
         rank=X,
         #Create a column that states which database allowed the ASVs to be assigned at the genus level
         presence=ifelse(!is.na(EuKribo_genus_2),ifelse(!is.na(PR2_Genus),"Présent dans les deux","EuKribo"),ifelse(!is.na(PR2_Genus),"PR2","Aucun")))

#Plot a diagram that represent the cumulative relative abundance of the ASVs depending on thier rank in term of relative abundance 
plot<-ggplot()+
  geom_point(mapping=aes(x=rank,y=CumSumAb,col=presence,size=Abd_rel),data=Table_plot)+
   scale_color_manual(values = c("Aucun" = "grey","PR2"="blue", "EuKribo" = "chartreuse3","Présent dans les deux"="firebrick"))+
  ylim(0,100)+
  xlim(0,147)+
  xlab("Rangs d'abondance")+
  ylab("Somme des abondances relatives cumulées")+
  labs(colour = "Présence dans les bases \n de référence")+
  labs(size = "Abondance relative (en %)")

#Save the plot 
ggsave("Output/plotTaxo.pdf",plot,width = 8, height = 4)



