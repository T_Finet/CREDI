#!/bin/bash
#
#SBATCH --partition long
#SBATCH --cpus-per-task 1
#SBATCH --mem 20GB

module load r/4.1.1
export TMPDIR=tmp/

Rscript Script/03_Training_Taxonomy_Bigger.R